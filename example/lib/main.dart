import 'dart:async';

import 'package:flutter/material.dart';
import 'package:generic_country_list_library/generic_country_list_library.dart';
import 'package:generic_country_list_library/widget_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Generic Picker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamController<List<WidgetModel>> _streamController = StreamController();
  var widgetList = <WidgetModel>[];

  @override
  void initState() {
    addWidgetData();
    super.initState();
  }


  void addWidgetData() {
    //Adding country field
    widgetList.add(getCountryWidgetData());

    //Adding state field
    widgetList.add(getStateWidgetData());

    //Adding district field
    widgetList.add(getDistrictWidgetData());

    //Adding city field
    widgetList.add(getCityWidgetData());

    _streamController.sink.add(widgetList);
  }

  WidgetModel getCountryWidgetData()
  {
    return WidgetModel(name : "Select Country",
        url : "",
        idFieldName: "countryId",
        nameFieldName: "countryName",
        hintString: "Choose Country",
        param: "",
        indexPosition: 0);
  }

  WidgetModel getStateWidgetData()
  {
    return WidgetModel(name : "Select State",
        url : "",
        idFieldName: "stateId",
        nameFieldName: "stateName",
        hintString: "Choose State",
        param: "",
        indexPosition: 1);
  }

  WidgetModel getDistrictWidgetData()
  {
    return WidgetModel(name : "Select District",
        url : "",
        idFieldName: "districtId",
        nameFieldName: "districtDesc",
        hintString: "Choose District",
        param: "",
        indexPosition: 2);
  }

  WidgetModel getCityWidgetData()
  {
    return WidgetModel(name : "Select City",
        url : "",
        idFieldName: "cityMasterId",
        nameFieldName: "cityName",
        hintString: "Choose City",
        param: "",
        indexPosition: 3);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Flutter TextField Example'),
        ),
        body: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                Flexible(
                  child: StreamBuilder<List<WidgetModel>>(
                      stream: _streamController.stream,
                      builder: (context,snapdata){
                        if (snapdata.hasData) {
                          return ListView.builder(
                              shrinkWrap: true,
                              itemCount: snapdata.data!.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: <Widget>[
                                    SizedBox(height: 10.0),
                                    Padding(
                                      padding: EdgeInsets.all(10),
                                      child: InkWell(
                                        onTap: () {
                                          _navigateAndDisplaySelection(context,snapdata.data!);
                                        },
                                        child: Container(
                                          width: double.infinity,
                                          padding: const EdgeInsets.all(15.0),
                                          decoration: BoxDecoration(
                                              border: Border.all(color: Colors.black),
                                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                          ),
                                          child: Text(snapdata.data![index].selectedData == null ? snapdata.data![index].name
                                              : snapdata.data![index].selectedData!.name,
                                          style: TextStyle(color: Colors.black54,
                                          fontSize: 15,fontWeight: FontWeight.w500)),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              });
                        }
                        else
                        {return Text("");}
                      }
                  ),
                ),
                TextButton(
                  child: Text(' DISPLAY ', style: TextStyle(fontSize: 20.0,color: Colors.white)),
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blue)),
                  onPressed: (){
                    String message  = "";
                    for (var i = 0; i < widgetList.length; i++) {
                      message = message+"${widgetList[i].name}\n\t\t\t\t${widgetList[i].idFieldName} = ${widgetList[i].selectedData?.id}\n\t\t\t\t${widgetList[i].nameFieldName} = ${widgetList[i].selectedData?.name}\n";
                    }
                    showAlertDialog(context, message, "DemoApp" , "Ok", "Cancel");
                  },
                )
              ],
            )
        )
    );
  }

  Future<void> _navigateAndDisplaySelection(BuildContext context, List<WidgetModel> widgetList) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => GenericDropDownpicker(widgetData: widgetList),
    )) as List<WidgetModel>;

    // When a BuildContext is used from a StatefulWidget, the mounted property
    // must be checked after an asynchronous gap.
    if (!mounted) return;
    for (var i = 0; i < widgetList.length; i++) {
      widgetList[i] = result[i];
    }

    _streamController.sink.add(widgetList);
  }

  showAlertDialog(BuildContext context, String message, String heading,
      String buttonAcceptTitle, String buttonCancelTitle) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text(buttonCancelTitle),
      onPressed: () {
        Navigator.of(context, rootNavigator: true)
            .pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text(buttonAcceptTitle),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(heading),
      content: Text(message),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  @override
  void dispose() {
    // stop streaming when app close
    _streamController.close();
  }
}

