#import "GenericCountryListLibraryPlugin.h"
#if __has_include(<generic_country_list_library/generic_country_list_library-Swift.h>)
#import <generic_country_list_library/generic_country_list_library-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "generic_country_list_library-Swift.h"
#endif

@implementation GenericCountryListLibraryPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftGenericCountryListLibraryPlugin registerWithRegistrar:registrar];
}
@end
