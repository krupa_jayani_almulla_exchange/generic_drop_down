library generic_country_list_library;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:generic_country_list_library/widget_model.dart';
import 'package:http/http.dart' as http;
import 'api_data_model.dart';

class GenericDropDownpicker extends StatefulWidget {
  final List<WidgetModel> widgetData;

  GenericDropDownpicker(
      {Key? key,
        required this.widgetData})
      : super(key: key);

  @override
  _GenericDropDownpickerState createState() => _GenericDropDownpickerState();
}

class _GenericDropDownpickerState extends State<GenericDropDownpicker> {
  List<DataModel> _dataList = [];
  StreamController<List<DataModel>> _streamController = StreamController();

  StreamController<List<Widget>> _streamWidgetController = StreamController();

  @override
  void initState() {
    getUpdatedWigdet(0);
    super.initState();
  }

  Future<http.Response> fetchDataFromAPI(String url, String param) {
    return http.get(Uri.parse(url + param), headers: {"Accept": "application/json"});
  }

  void callApi(String url, String param, String nameFieldName,
      String idFieldName) async {
    _dataList.clear();
    _streamController.sink.addError('error loading');
    var response = await fetchDataFromAPI(url, param);
    var json = jsonDecode(response.body);
    if (json['data'] != null) {
      json['data'].forEach((v) {
        var name = v[nameFieldName].toString();
        var id = v[idFieldName].toInt();
        _dataList.add(DataModel(name: name, id: id));
      });
    }
    _streamController.sink.add(_dataList);
  }

  void setEmptyList() async {
    _dataList.clear();
    _streamController.sink.add(_dataList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Choose Country'),
        ),
        body: Padding(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 15),
          child: Flexible(
              child: StreamBuilder<List<Widget>>(
                  stream: _streamWidgetController.stream,
                  builder: (context, snapdata) {
                    if (snapdata.hasData) {
                      return Column(children: snapdata.data!);
                    } else {
                      return Text("");
                    }
                  })),
        ));
  }

  void getUpdatedWigdet(int type) {
    var widgetList = <Widget>[];
    if(type == widget.widgetData.length)
      {
        for (var i = 0; i <= type-1; i++) {
          widgetList.add(getInputWidget(i));
        }
      }
    else
      {
        for (var i = 0; i <= type; i++) {
          widgetList.add(getInputWidget(i));
        }
      }

    if(type == widget.widgetData.length) {
      widgetList.add(getSubmitButton());
      setEmptyList();
      widgetList.add(getListWidget(type));
    }
    else
      {
        callApi(widget.widgetData[type].url, widget.widgetData[type].param,
            widget.widgetData[type].nameFieldName, widget.widgetData[type].idFieldName);
        widgetList.add(getListWidget(type));
      }

    _streamWidgetController.sink.add(widgetList);
    if(type != widget.widgetData.length)
    {
      widget.widgetData[type].unitFocusNode.requestFocus();
    }

  }

  Widget getInputWidget(int index) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: TextField(
        onTap: (){
          widget.widgetData[index].txtInputController.clear();
          getUpdatedWigdet(index);
        },
        controller: widget.widgetData[index].txtInputController,
        focusNode: widget.widgetData[index].unitFocusNode,
        onChanged: (value) {
          List<DataModel> datafilter = _dataList
              .where((i) => i.name.contains(value.toUpperCase()))
              .toList();
          if(value != "")_streamController.sink.add(datafilter);
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: widget.widgetData[index].name,
          hintText: widget.widgetData[index].hintString,
        ),
      ),
    );
  }

  Widget getSubmitButton() {
    return TextButton(
      child: Text(' SUBMIT ', style: TextStyle(fontSize: 20.0,color: Colors.white)),
      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blue)),
      onPressed: (){
        Navigator.pop(context, widget.widgetData);
      },
    );
  }

  Widget getListWidget(int type) {
    return Flexible(
      child: StreamBuilder<List<DataModel>>(
          stream: _streamController.stream,
          builder: (context, snapdata) {
            if (snapdata.hasData) {
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapdata.data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        //Close keyboard
                        FocusManager.instance.primaryFocus?.unfocus();

                        //Set selected data and parameter to next api call
                        WidgetModel widgetModel = widget.widgetData[type];
                        widgetModel.selectedData = snapdata.data![index];
                        widget.widgetData[type] = widgetModel;
                        if(widget.widgetData.length != type+1) {
                          widget.widgetData[type+1].setParameter(type+1, snapdata.data![index].id, widget.widgetData[1].selectedData != null ? widget.widgetData[1].selectedData!.id : 0);
                        }

                        //Set text to text input field
                        widget.widgetData[type].txtInputController.text = snapdata.data![index].name;
                        //Clear data
                        for (var i = type+1; i < widget.widgetData.length; i++) {
                          if(widget.widgetData.length != type+1)
                            {
                              widget.widgetData[i].txtInputController.clear();
                            }
                        }
                        //Update widget
                        getUpdatedWigdet(type+1);
                      },
                      child: Container(
                        width: double.infinity,
                        padding: const EdgeInsets.all(15.0),
                        child: Text('${snapdata.data![index].name}',
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 15,
                                fontWeight: FontWeight.w500)),
                      ),
                    );
                  });
            } else if (snapdata.hasError) {
              return LinearProgressIndicator();
            } else {
              return SizedBox.shrink();
            }
          }),
    );
  }

  @override
  void dispose() {
    // stop streaming when app close
    _streamController.close();
    _streamWidgetController.close();
  }
}
