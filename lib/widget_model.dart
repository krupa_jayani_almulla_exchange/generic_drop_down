import 'package:flutter/cupertino.dart';
import 'package:generic_country_list_library/api_data_model.dart';

class WidgetModel {
  String name;
  String url;
  String idFieldName;
  String nameFieldName;
  String hintString;
  String param;
  int indexPosition;
  DataModel? selectedData;
  TextEditingController txtInputController = TextEditingController();
  final FocusNode unitFocusNode = FocusNode();

  WidgetModel({
    required this.name,
    required this.url,
    required this.idFieldName,
    required this.nameFieldName,
    required this.hintString,
    required this.param,
    required this.indexPosition,
    this.selectedData,
  });

  void setParameter(int index,int fieldValue1, int fieldValue2)
  {
    if(index == 1)
    {
      param = "countryId=$fieldValue1";
    } else if(index == 2)
    {
      param = "stateId=$fieldValue1";
    } else if(index == 3)
    {
      param = "districtId=$fieldValue1+&stateId=$fieldValue2";
    }
  }
}

